<h1 style="text-align: center !important;">icee-container-rs</h1>

<br />

<div align="center">
  <a href="https://crates.io/crates/icee-container-rs">
    <img src="https://img.shields.io/crates/v/icee-container-rs.svg?style=flat-square"
    alt="Crates.io" />
  </a>
  <a href="https://docs.rs/icee-container-rs">
    <img src="https://img.shields.io/badge/docs-latest-blue.svg?style=flat-square"
      alt="docs.rs" />
  </a>
</div>

<br />

Container service for custom DI

- Global container accessed with ["get", "has", "set"] methods
- Local typed container storing services under async::sync::Arc

