use thiserror::Error;

#[derive(Error, Debug, PartialEq)]
pub enum Error {
    #[error("Missing service [{0}]")]
    NotFound(String),
    #[error("Incorrect Service type of [{0}]")]
    TypeMismatch(String),
}

// pub struct NotFound {
//     pub name: String,
// }
//
// #[derive(Error, Debug)]
// pub struct TypeMismatch {
//     pub name: String,
// }
