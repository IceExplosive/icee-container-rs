use std::any::Any;
use std::collections::HashMap;

use async_std::sync::{Arc, RwLock};
use lazy_static::lazy_static;

use crate::Error;

lazy_static! {
    static ref CONTAINER: RwLock<HashMap<&'static str, Arc<dyn Any + Send + Sync>>> =
        RwLock::new(HashMap::new());
}

/// Checks whether or not is given service registered
///
/// ```
/// # async_std::task::block_on(async {
/// use icee_container_rs::has;
///
/// let exists: bool = has("service_name").await;
/// # assert_eq!(exists, false);
/// # })
/// ```
pub async fn has(name: &str) -> bool {
    unsafe { (&CONTAINER).read().await.contains_key(name) }
}

/// Retrieves named service
///
/// To satisfy compiler it's also required to specify exact service type under which
/// it has been stored wrapped within [async_std::sync::Arc]
///
/// ```no_run
/// use icee_container_rs::get;
/// use async_std::sync::Arc;
///
/// struct Service {};
/// # async_std::task::block_on(async {
/// let service: Arc<Service> = get("svc").await.unwrap();
/// # })
/// ```
pub async fn get<T: Any + Send + Sync>(name: &str) -> Result<Arc<T>, Error> {
    unsafe {
        if has(name).await {
            let srv = (&CONTAINER)
                .read()
                .await
                .get(name)
                .unwrap()
                .clone()
                .downcast::<T>()
                .map_err(|_| Error::TypeMismatch(name.to_string()))?;

            return Ok(srv);
        }

        Err(Error::NotFound(name.to_string()))
    }
}

/// Stores named service
///
/// To make services singleton, each of them is wrappen within [async_std::sync::Arc]
///
/// ```
/// use icee_container_rs::set;
///
/// struct Service {};
/// # async_std::task::block_on(async {
/// set("svc", Service{}).await;
/// # })
/// ```
pub async fn set<T: Any + Send + Sync>(name: &'static str, service: T) {
    unsafe {
        (&CONTAINER).write().await.insert(name, Arc::new(service));
    }
}
