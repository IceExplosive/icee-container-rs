use async_std::sync::Arc;
use icee_container_rs::*;

#[async_std::test]
async fn test_container() {
    set::<usize>("val", 55).await;

    let v = get::<usize>("val").await.unwrap();
    assert_eq!(v, Arc::new(55));

    let v = get::<usize>("value").await;
    assert_eq!(v.is_err(), true);
    assert_eq!(v.unwrap_err(), Error::NotFound("value".into()));

    let v = get::<i8>("val").await;
    assert_eq!(v.is_err(), true);
    assert_eq!(v.unwrap_err(), Error::TypeMismatch("val".into()));
}
