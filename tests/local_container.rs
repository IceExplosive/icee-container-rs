use async_std::sync::Arc;
use icee_container_rs::{Container, Error};

#[test]
fn test_container() {
    let mut c = Container::<usize>::new();
    c.set("val", 666);

    let v = c.get("val").unwrap();
    assert_eq!(v, Arc::new(666));

    let v = c.get("value").unwrap_err();
    assert_eq!(v, Error::NotFound("value".into()));

    let v = c.set("val-2", 999);
    assert_eq!(v, Arc::new(999));

    let mut v = c.list();
    v.sort();
    assert_eq!(v, vec!["val", "val-2"]);
}
